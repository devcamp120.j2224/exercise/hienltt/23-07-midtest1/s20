package com.devcamp.restapi.controller;


import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Control {
    @CrossOrigin
    @GetMapping("/reverse-string")
    public String getReverseString(@RequestParam(required = true) String str) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();
        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }
        return strBuilder.toString();
    }

    @CrossOrigin
    @GetMapping("/isPalindrome")
    public String checkPalindrome(@RequestParam(required = true) String str) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();
        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }
        String reverseStr = strBuilder.toString();
        if (reverseStr.equals(str)) {
            return "Chuỗi " + str + " là chuỗi Palindrome";
        }
        else {
            return "Chuỗi " + str + " KHÔNG phải là chuỗi Palindrome";
        }
    }

    @CrossOrigin
    @GetMapping("/remove-dup-char")
    public String removeDuplicateChar(@RequestParam(required = true) String str) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < str.length(); i++) {
            if (!charsPresent.contains(str.charAt(i))) {
                stringBuilder.append(str.charAt(i));
                charsPresent.add(str.charAt(i));
            }
        }
        return stringBuilder.toString();
    }

    @CrossOrigin
    @GetMapping("/append-string") 
    public String getAppendStringTogether(@RequestParam(required = true) String str1, @RequestParam(required = true) String str2)
    {
        if (str1.length() == str2.length())
            return str1 + str2;
        if (str1.length() > str2.length())
        {
            int diff = str1.length() - str2.length();
            return str1.substring(diff, str1.length()) + str2;
        } else
        {
            int diff = str2.length() - str1.length();
            return str1 + str2.substring(diff, str2.length());
        }
    }



}
